/**
 * (c) 2012 TRIOLOGY GmbH
 */
/**
 * This package contains classes for the SCM-Htpasswd-Auth.
 * 
 * @author Ahmed Saad, TRIOLOGY GmbH
 */
package de.triology.scm.plugins.htpasswd;