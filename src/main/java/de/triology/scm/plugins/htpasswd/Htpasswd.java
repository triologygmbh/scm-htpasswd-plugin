/**
 * Copyright (c) 2013, TRIOLOGY GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.scm-manager.com
 *
 */

package de.triology.scm.plugins.htpasswd;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.codec.digest.Crypt;
import org.apache.commons.codec.digest.Md5Crypt;
import org.apache.shiro.crypto.hash.Sha1Hash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sonia.scm.user.User;
import sonia.scm.user.UserManager;
import sonia.scm.web.security.AuthenticationResult;

/**
 * 
 * @author Ahmed Saad, TRIOLOGY GmbH
 */
public class Htpasswd {

	/** the logger for Htpasswd. */
	private static final Logger LOGGER = LoggerFactory.getLogger(Htpasswd.class);

	/** Type name for users using this authentication method. */
	public static final String TYPE = "htpasswd";

	/** User database. */
	private final Map<String, User> userDB = new ConcurrentHashMap<String, User>();

	/** User Manager. */
	private UserManager userManager;

	/** Minimal length of encrypted password (CRYPT). */
	private static final int MIN_LENGTH = 13;

	/**
	 * Read the file and map data.
	 * 
	 * @param htpasswordFile .htpassword File
	 */
	public final void readHtpasswd(final File htpasswordFile) {

		if (htpasswordFile.isFile()) {
			LOGGER.trace("read file {}", htpasswordFile.getAbsoluteFile());
			clearDB();
			try {
				final FileInputStream fis = new FileInputStream(htpasswordFile);
				final BufferedInputStream bis = new BufferedInputStream(fis);
				final BufferedReader dis = new BufferedReader(new InputStreamReader(bis));
				int lineNr = 1;
				String[] userData;
				String line = dis.readLine();
				while (null != line) {
					if (!line.isEmpty()) {
						userData = line.split(":", 2);
						String name = "";
						String pwd = "";
						if (userData.length >= 2) {
							name = userData[0];
							pwd = userData[1];
						}

						if (name.length() >= 1 && pwd.length() >= MIN_LENGTH) {
							addUser(name, pwd);
						} else {
							LOGGER.warn(".htpasswd file line ({}): No user name or password is too short.", lineNr);
						}
					}
					line = dis.readLine();
					lineNr++;
				}
				fis.close();
				bis.close();
				dis.close();

			} catch (final FileNotFoundException e) {
				LOGGER.error("Error loading .htpasswd file.", e);
			} catch (final IOException e) {
				LOGGER.error("Error while processing .htpasswd file.", e);
			}
		} else {
			LOGGER.error("{} is not a file.", htpasswordFile.getAbsolutePath());
		}
	}

	/**
	 * clear user DB.
	 */
	public final void clearDB() {
		userDB.clear();
	}

	/**
	 * Authenticate.
	 * 
	 * @param username Username
	 * @param password Password
	 * 
	 * @return AuthenticationResult
	 */
	public final AuthenticationResult authenticate(final String username, final String password) {
		AuthenticationResult result = null;
		User user = getUserFromManager(username);
		String htpasswdPassword = "";
		String md5Crypt = "";
		String sha1Hash = "";
		String crypt = "";
		String salt = "";

		if (user != null) {
			htpasswdPassword = user.getPassword();
			if (htpasswdPassword.startsWith("$apr1$")) {
				String[] md5Key = htpasswdPassword.split("[$]");
				salt = md5Key[2];
				md5Crypt = Md5Crypt.apr1Crypt(password, salt);

			} else if (htpasswdPassword.startsWith("{SHA}")) {
				htpasswdPassword = htpasswdPassword.substring(htpasswdPassword.indexOf("}") + 1);
				sha1Hash = new Sha1Hash(password).toBase64();

			} else if (htpasswdPassword.length() > 2) {
				salt = htpasswdPassword.substring(0, 2);
				crypt = Crypt.crypt(password, salt);
			}

			if (htpasswdPassword.equals(md5Crypt) || htpasswdPassword.equals(crypt)
					|| MessageDigest.isEqual(htpasswdPassword.getBytes(), sha1Hash.getBytes())) {

				result = new AuthenticationResult(user);
			} else {
				result = AuthenticationResult.FAILED;
			}
		} else {
			result = AuthenticationResult.NOT_FOUND;
		}

		return result;
	}

	/**
	 * @param username User name
	 * @return User object
	 */
	private User getUserFromManager(final String username) {
		User user = null;
		if (!userDB.isEmpty()) {
			user = userDB.get(username);
			String pwd = user.getPassword();

			if (userManager.contains(username) && TYPE.equals(userManager.get(username).getType())) {
				user = userManager.get(username);
				user.setPassword(pwd);
			}

		}
		return user;
	}

	/**
	 * Adds a user to DB.
	 * 
	 * @param username User name
	 * @param pwd Password
	 */
	private void addUser(final String username, final String pwd) {
		User user = new User(username, username, "");
		user.setType(TYPE);
		user.setPassword(pwd);
		userDB.put(user.getName(), user);
	}

	/**
	 * 
	 * @return TYPE
	 */
	public final String getType() {
		return TYPE;
	}

	/**
	 * @return the userManager
	 */
	public final UserManager getUserManager() {
		return userManager;
	}

	/**
	 * @param pUserManager the userManager to set
	 */
	public final void setUserManager(final UserManager pUserManager) {
		this.userManager = pUserManager;
	}
}
