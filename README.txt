scm-htpasswd-plugin
==============================
This plugin is for usage with the SCM-Manager project (https://bitbucket.org/sdorra/scm-manager).

Issue tracker: https://bitbucket.org/triologygmbh/scm-manager-plugins/issues
Please set component to htpasswd-plugin.

Wiki: https://bitbucket.org/triologygmbh/scm-manager-plugins/wiki/Htpasswd

Support that makes the difference
https://www.scm-manager.com/support